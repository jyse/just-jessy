class RecipesController < ApplicationController
	def index
		@recipes= Recipe.all 
	end

	def new
	end

	def show
		@recipe = Recipe.find(params[:id])
	end

	def create
		@recipe = Recipe.new(recipe_params)
		@recipe.save
		redirect_to @recipe
	end

	private
	  	def recipe_params
			params.require(:recipe).permit(:course, :name, :ingredients, :duration, :description)
		end	
end